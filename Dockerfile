FROM galaxy/galaxy-init:|glxyvrsn|
#FROM galaxy/galaxy-init:v18.01

MAINTAINER walzer@ebi.ac.uk

LABEL Description="Galaxy |glxyvrsn| - OpenMS |opnmsvrsn| integrations"
LABEL software="Galaxy Init"
LABEL software.version="|glxyvrsn|"
LABEL version="|opnmsvrsn|"

# Basic galaxy integration control
COPY galaxy-settings/config/job_conf.xml /galaxy-export/config/job_conf.xml 
COPY galaxy-settings/config/tool_conf.xml /galaxy-export/config/tool_conf.xml
COPY galaxy-settings/tools/openms /galaxy-export/tools/openms

#COPY galaxy-settings/config/container_resolvers_conf.xml config/container_resolvers_conf.xml  # not at the moment: bioconda -> mulled container
#COPY galaxy-settings/config/sanitize_whitelist.txt config/sanitize_whitelist.txt  # not at the moment

# Reqs/limits
#COPY galaxy-settings/config/job_resource_params_conf.xml config/job_resource_params_conf.xml

# Galaxy tours which guide users through the subsequent steps in an analysis
# COPY galaxy-settings/config/plugins/tours/*.yaml config/plugins/tours/

COPY galaxy-settings/welcome /galaxy-export/welcome

# COPY workflows workflows_to_load
# COPY post-start-actions.sh post-start-actions.sh
# RUN chmod u+x post-start-actions.sh
