# galaxy-integration-container
Create galaxy init container images with OpenMS integration for use with [docker-galaxy-stable](https://github.com/bgruening/docker-galaxy-stable) and [kubernetes deployments](https://github.com/galaxyproject/galaxy-kubernetes)

Gets the OpenMS integrations [from the integration repo](https://gitlab.ebi.ac.uk/walzer/openms-integrations/tags). 
Parameters for the container image build are:
  * GalaxyVersion: "v18.01" (has to be an existing base tag for docker `FROM galaxy/galaxy-init`)
  * GalaxyIntegration: "https://gitlabci.ebi.ac.uk/walzer/openms-integrations/uploads/43199854c2d2c3a79cb41af9c7f17e47/artifacts_V2.3.0.zip" (the integration artifacts obviously, can also come from outside sources)
  * OpenMSversion: "V2.3.0" (should reflect the Galaxy integration choice, is used for the container image tag)

Trigger a build with `https://gitlab.ebi.ac.uk/api/v4/projects/830/ref/REF_NAME/trigger/pipeline?token=TOKEN&variables[GalaxyVersion]=v18.01&variables[OpenMSversion]=v18.01` where TOKEN is the Pipeline trigger's token, REF_NAME the branch, and the container specific parameters are set via variables[..]. See gitlab [doc on pipeline triggers](https://gitlab.ebi.ac.uk/walzer/galaxy-integration-container/settings/ci_cd) for more.